from django.views.decorators.csrf import csrf_exempt
from django.http import JsonResponse
from django.conf import settings
from django.contrib.staticfiles.finders import find
from django.templatetags.static import static

import os
from pathlib import Path
import numpy as np
from .cbl_pytorch.tool.inference import infer_one

# Create your views here.
def get_static(path):
    file = None
    if settings.DEBUG:
        file = find(path)
    else:
        file = static(path)
    
    if not file:
        base_dir = str(Path(__file__).resolve().parent.parent) + "/staticfiles/"
        file = base_dir + path
    return file

config_filepath = get_static("contrastBoundary/main_conf.yaml")


def ping_server(request):
    return JsonResponse({'status' : 'OK'})


def test_infer(request):
    dummy_file = get_static("contrastBoundary/Area_1_office_1.npy")
    data = np.load(dummy_file)
    result, _ = infer_one(data, config_filepath)
    print("Result:", result.shape)
    return JsonResponse({'status' : 'OK', 'data' : result.tolist()})


@csrf_exempt
def infer_file(request):
    if(request.method == 'POST'):
        assert('file' in request.FILES)

        print('Sent file:', request.FILES['file'])
        text_data = request.FILES['file'].readlines()
        for i in range(len(text_data)):
            text_data[i] = text_data[i].decode("utf-8").strip().split(" ")

        data = np.array(text_data).astype(np.float32)
        result, _ = infer_one(data, config_filepath)
        print("Result:", result.shape)

        return JsonResponse({'status' : 'OK', 'data' : result.tolist()})
    return JsonResponse({'status' : 'ERROR', 'message' : 'use POST to access'})


@csrf_exempt
def infer_data(request):
    if(request.method == 'POST'):
        assert('data' in request.POST)

        data = np.array(request.POST['data']).astype(np.float32)
        result, _ = infer_one(data, config_filepath)
        print("Result:", result.shape)

        return JsonResponse({'status' : 'OK', 'data' : result.tolist()})
    return JsonResponse({'status' : 'ERROR', 'message' : 'use POST to access'})


