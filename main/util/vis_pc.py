import numpy as np
import open3d as o3d
import argparse
import pickle
from plyfile import PlyData

def read_ply(filename):
	try:
		pcd = o3d.io.read_point_cloud(filename)
		point = np.asarray(pcd.points)
		color = np.asarray(pcd.colors)

		assert(point.shape[0] == color.shape[0])

		x_len, _ = point.shape
		pcd_np = np.zeros((x_len, 7))
		pcd_np[:, 0:3] = point[:, 0:3]
		pcd_np[:, 3:6] = color[:, 0:3]
		return pcd_np
	except:
		pcd = PlyData.read(filename)
		x_len = len(pcd.elements[0])
		y_len = len(pcd.elements[0].properties)
		print("Size: (%d, %d)" % (x_len, y_len))

		pcd_np = np.zeros((x_len, y_len))
		for i in range(y_len):
			name = pcd.elements[0].properties[i].name
			data = pcd.elements[0].data[name]
			pcd_np[:, i] = np.array(data)
			print("Property:", name)
		return pcd_np

def read_npy(filename):
	data = np.load(filename)
	print(data.shape)
	return data

def read_txt(filename):
	result = []
	with open(filename) as f:
		for line in f:
			split_line = line.split()
			for i in range(len(split_line)):
				split_line[i] = float(split_line[i])
			result.append(split_line)
	
	result = np.array(result)
	return result

def read_pkl(filename):
	data = pickle.load(open(filename, 'rb'))

	pc, _ = data
	pc = pc[~np.isnan(pc).any(1)]
	print(pc.shape)
	return pc

def np_to_pcd(data):
	pcd = o3d.geometry.PointCloud()
	pcd.points = o3d.utility.Vector3dVector(data[:, 0:3])

	lim_max = np.amax(data[:, 3:6])
	if(lim_max <= 1):
		pcd.colors = o3d.utility.Vector3dVector(data[:, 3:6])
		# data[:, 3:6] = np.round(data[:, 3:6] * 255).astype(np.int32)
	else:
		pcd.colors = o3d.utility.Vector3dVector(data[:, 3:6] / 255)
	print(np.unique(data[:, 3:6], axis=0))

	if(data.shape[1] == 7):
		print(np.unique(data[:, 6], axis=0))

	return pcd

def translate_label(data):
	g_colors = [[0,255,0], [0,0,255], [0,255,255], [255,255,0], [255,0,255],
        [100,100,255], [200,200,100], [170,120,200], [255,0,0], [200,100,100],
        [10,200,100], [200,200,200], [50,50,50]]
	
	for i in range(data.shape[0]):
		data[i, 3:6] = g_colors[int(data[i, 6])]
	return data


def normalize_shape(data):
	x, y = data.shape
	new_data = np.zeros((x, 7))
	min_val = min(y, 7)
	new_data[:, :min_val] = data[:, :min_val]
	return new_data


def visualize(pcd, volume=False):
	if(volume):
		try:
			hull, _ = pcd.compute_convex_hull()
			hull.orient_triangles()
			volume = hull.get_volume()
			print(volume)
		except:
			print("Volume Error")
	o3d.visualization.draw_geometries([pcd])

if __name__ == "__main__":
	parser = argparse.ArgumentParser()
	parser.add_argument('--filename', type=str, default='')
	parser.add_argument('--use_label', type=bool, default=False)
	parser.add_argument('--volume', type=bool, default=False)
	args = parser.parse_args()

	if(args.filename[-4:] == ".ply"):
		pcd = read_ply(args.filename)
	elif(args.filename[-4:] == ".txt"):
		pcd = read_txt(args.filename)
	elif(args.filename[-4:] == ".npy"):
		pcd = read_npy(args.filename)
	elif(args.filename[-4:] == ".pkl"):
		pcd = read_pkl(args.filename)
	else:
		print("File not compatible")
		exit()
	
	if(args.use_label and pcd.shape[1] == 7):
		pcd = translate_label(pcd)
	pcd = np_to_pcd(pcd)
	visualize(pcd, volume=args.volume)