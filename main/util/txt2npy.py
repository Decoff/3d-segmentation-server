import numpy as np
from plyfile import PlyData
import glob

def txt2npy(source, target):
	result = []
	with open(source) as f:
		for line in f:
			split_line = line.split()
			for i in range(len(split_line)):
				split_line[i] = float(split_line[i])
			result.append(split_line)
	
	result = np.array(result)
	np.save(target, result)
	print("Saved file", source, "into", target)

def npy2txt(source, target):
	data = np.load(source)
	with open(target, 'w') as f:
		for line in data:
			str_line = ""
			for item in line:
				str_line += str(item) + " "
			str_line = str_line.strip() + "\n"
			f.write(str_line)

if __name__ == "__main__":
	files = [f[:-4] for f in glob.glob("*.txt")]
	sources = [f + ".txt" for f in files]
	targets = ["data/" + f + ".npy" for f in files]

	print(files)
	print(sources)
	print(targets)

	for source, target in zip(sources, targets):
		txt2npy(source, target)
