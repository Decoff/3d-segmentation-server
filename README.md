# 3D Object Segmentation with Edge Computing via iPhone - Server

This is the repository for server code of final project "3D Object Segmentation with Edge Computing via iPhone" written by Adrian Kaiser and Nadhif Adyatma Prayoga as its authors.

## Acknowledgements

This repository contains implementation of contrastive boundary learning method. Code for contrastive boundary is taken from the implementation of the original paper, **Contrastive Boundary Learning for Point Cloud Segmentation** [[arXiv](https://arxiv.org/abs/2203.05272)]. The code for contrastive boundary is taken from [this repository](https://github.com/LiyaoTang/contrastBoundary).

## Instructions

- Run server

```
pip install -r requirements.txt
python manage.py runserver
```

NOTE: In order to use the server from within the application, you must edit the server link on the app manually. The application can be found in [this repository](https://github.com/arizbw/PointCloudSegmenter).
