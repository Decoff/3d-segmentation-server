from django.urls import path
from .views import infer_file, infer_data, test_infer

urlpatterns = [
    path('cbl/test/', test_infer, name ='test_infer'),
    path('cbl/inference/', infer_file, name ='infer_file'),
    path('cbl/inferrence-data/', infer_data, name ='infer_data')
]
