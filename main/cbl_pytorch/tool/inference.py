import os, sys
import glob
import time
import random
import numpy as np
import pickle
import argparse
import collections

DIR_PATH = os.path.dirname(__file__)
ROOT_PATH = os.path.dirname(DIR_PATH)
sys.path.insert(0, DIR_PATH)
sys.path.insert(0, ROOT_PATH)

import torch
import torch.nn as nn
import torch.nn.parallel
import torch.optim
import torch.utils.data

# from sklearn.metrics import confusion_matrix
# conf = confusion_matrix(label[mask], pred[mask], labels=np.arange(config.num_classes))

from util import config
from util.common_util import AverageMeter, intersectionAndUnion, check_makedirs
from util.voxelize import voxelize
from util.logger import *
from model.basic_operators import _eps, _inf
from model.utils import *
from lib.pointops.functions import pointops

random.seed(123)
np.random.seed(123)

def read_npy(filename):
	data = np.load(filename)
	return data

def read_txt(filename):
	result = []
	with open(filename) as f:
		for line in f:
			split_line = line.split()
			for i in range(len(split_line)):
				split_line[i] = float(split_line[i])
			result.append(split_line)
	
	result = np.array(result)
	return result

def read_pkl(filename):
	data = pickle.load(open(filename, 'rb'))

	pc, _ = data
	pc = pc[~np.isnan(pc).any(1)]
	print(pc.shape)
	return pc


def translate_label(data):
	g_colors = [[0,255,0], [0,0,255], [0,255,255], [255,255,0], [255,0,255],
		[100,100,255], [200,200,100], [170,120,200], [255,0,0], [200,100,100],
		[10,200,100], [200,200,200], [50,50,50]]
	
	for i in range(data.shape[0]):
		data[i, 3:6] = g_colors[int(data[i, 6])]
	return data


def normalize_shape(data):
	x, y = data.shape
	new_data = np.zeros((x, 7))
	min_val = min(y, 7)
	new_data[:, :min_val] = data[:, :min_val]
	return new_data


def get_index_array(array1, array2):
	arr1_x = array1.shape[0]
	arr2_x = array2.shape[0]

	indexes = []
	for i in range(arr1_x):
		comp_obj = array1[i, ...]
		
	return indexes


def sample_data(data, num_sample):
    """ data is in N x ...
        we want to keep num_samplexC of them.
        if N > num_sample, we will randomly keep num_sample of them.
        if N < num_sample, we will randomly duplicate samples.
    """
    N = data.shape[0]
    if (N == num_sample):
        return data, range(N)
    elif (N > num_sample):
        sample = np.random.choice(N, num_sample)
        return data[sample, ...], sample
    else:
        sample = np.random.choice(N, num_sample-N)
        dup_data = data[sample, ...]
        return np.concatenate([data, dup_data], 0), list(range(N))+list(sample)

def _try_eval(s):
	try:
		s = eval(s)
	except:
		pass
	return s

def get_parser():
	parser = argparse.ArgumentParser(description='PyTorch Point Cloud Semantic Segmentation')
	parser.add_argument('--config', type=str, default='config/s3dis/s3dis_pointtransformer_repro.yaml', help='config file')
	parser.add_argument('--dpath', type=str, default=None)
	parser.add_argument('--spath', type=str, default=None)
	parser.add_argument('--restrict', type=str, default=None)
	parser.add_argument('--set', type=str, default=None, help='command line setting k-v tuples')
	parser.add_argument('opts', help='see config/s3dis/s3dis_pointtransformer_repro.yaml for all options', default=None, nargs=argparse.REMAINDER)
	args = parser.parse_args()
	assert args.config is not None
	assert args.dpath is not None
	assert args.spath is not None

	cfg = config.load_cfg_from_cfg_file(args.config)
	cfg = config.CfgNode(cfg, default='')
	return cfg, args


def run_from_py(config, _dpath, _spath, restrict=None):
	random.seed(123)
	np.random.seed(123)

	parser = argparse.ArgumentParser()
	args = parser.parse_args()

	args.config = config
	args.dpath = _dpath
	args.spath = _spath
	args.restrict = restrict

	cfg = config.load_cfg_from_cfg_file(args.config)
	cfg = config.CfgNode(cfg, default='')

	args, xargs = cfg, args
	dpath = xargs.dpath
	spath = xargs.spath
	model, criterion, names = prep_test(args)
	infer_path(model, criterion, names, args)


def main():
	random.seed(123)
	np.random.seed(123)
	# os.environ['CUDA_VISIBLE_DEVICES']='7'
	# os.environ['CUDA_LAUNCH_BLOCKING']='1'
	args, xargs = get_parser()
	dpath = xargs.dpath
	spath = xargs.spath
	model, criterion, names = prep_test(args)
	infer_path(model, criterion, names)

def prep_test(args):
	if args.arch == 'pointtransformer_seg_repro':
		from model.pointtransformer_seg import pointtransformer_seg_repro as Model
	else:
		raise Exception('architecture not supported yet'.format(args.arch))
	model = Model(c=args.fea_dim, k=args.classes, config=args).cuda()

	from model.pointtransformer_seg import Loss
	# criterion = Loss(config=args).cuda()
	criterion = nn.CrossEntropyLoss(ignore_index=args.ignore_label).cuda()
	names = ['ceiling','floor','wall','beam','column','window','door','chair','table','bookcase','sofa','board','clutter']
	if os.path.isfile(args.model_path):
		checkpoint = torch.load(args.model_path)
		state_dict = checkpoint['state_dict']
		new_state_dict = collections.OrderedDict()
		for k, v in state_dict.items():
			name = k[7:]
			new_state_dict[name] = v
		model.load_state_dict(new_state_dict, strict=True)
		args.epoch = checkpoint['epoch']
	else:
		raise RuntimeError("no checkpoint found at '{}'".format(args.model_path))
	return model, criterion, names


def data_load(data_name, args):
	# load the whole cloud - coord, feat, label
	# idx_data - enmerate over pts selection in voxel
	# if(data_name[-4:] == ".ply"):
	# 	data = read_ply(data_name)
	if(data_name[-4:] == ".txt"):
		data = read_txt(data_name)
	elif(data_name[-4:] == ".npy"):
		data = read_npy(data_name)
	elif(data_name[-4:] == ".pkl"):
		data = read_pkl(data_name)
	else:
		print("File not compatible")
		exit()
	data = normalize_shape(data)

	if(data.shape[0] > 400000):
		data, _ = sample_data(data, 400000)
	coord, feat, label = data[:, :3], data[:, 3:6], data[:, 6]

	print("Data shape:", data.shape)

	idx_data = []
	if args.voxel_size:
		coord, feat = input_normalize(coord, feat)
		idx_sort, count = voxelize(coord, args.voxel_size, mode=1)  # sorted idx & cnts of entire cloud
		for i in range(count.max()):
			# enumerate through the max num of pts in the same voxel - [N_vx]
			idx_select = np.cumsum(np.insert(count, 0, 0)[0:-1]) + i % count
			idx_part = idx_sort[idx_select]
			idx_data.append(idx_part)
	else:
		idx_data.append(np.arange(label.shape[0]))
	# [N, 3], [N, 3], [N], [[N_vx], ...]
	return coord, feat, label, idx_data


def input_normalize(coord, feat):
	coord_min = np.min(coord, 0)
	coord -= coord_min
	if(np.max(feat) > 1):
		feat = feat / 255.
	return coord, feat


def infer_path(model, criterion, names, args):
	global dpath, spath
	args.batch_size_test = 10
	model.eval()

	print("Data path:", dpath)
	print("Save path:", spath)
	check_makedirs(spath)

	if(not args.restrict):
		data_list = [file for file in glob.glob(dpath + "/*.npy")]
		data_list.extend([file for file in glob.glob(dpath + "/*.txt")])
		data_list.extend([file for file in glob.glob(dpath + "/*.pkl")])
	else:
		data_list = [file for file in glob.glob(dpath + args.restrict)]
	print("Data list:", len(data_list))
	for idx, item in enumerate(data_list):
		print("Current file:", item)
		
		start_time = time.time()
		item_file = item.split("/")[-1][:-4]

		coord, feat, _, idx_data = data_load(item, args)
		idx_size = len(idx_data)
		print("Index data len:", idx_size)

		curr_dict = {
			'pred' : np.zeros((coord.shape[0], 13)),
			'pred_last' : np.zeros((coord.shape[0], 13))
		}

		# prepare into multiple input sample(s)
		idx_list, coord_list, feat_list, offset_list  = [], [], [], []
		for i in range(idx_size):  # for each enmeration of the full (potentially voxelized) cloud
			#logger.info('{}/{}: {}/{}/{}, {}'.format(idx + 1, len(data_list), i + 1, idx_size, idx_data[0].shape[0], item))
			idx_part = idx_data[i]

			# select points from full cloud into voxels
			coord_part, feat_part = coord[idx_part], feat[idx_part]

			if args.voxel_max and coord_part.shape[0] > args.voxel_max:
				# print(f"{i} IF shape exceeds")
				# if exceeding test/val voxel num - spatially regular gen
				coord_p, idx_uni, _ = np.random.rand(coord_part.shape[0]) * 1e-3, np.array([]), 0
				# until all voxels of current cloud covered
				while idx_uni.size != idx_part.shape[0]:
					# select center (min potentials)
					init_idx = np.argmin(coord_p)
					# crop by distance
					dist = np.sum(np.power(coord_part - coord_part[init_idx], 2), 1)
					idx_crop = np.argsort(dist)[:args.voxel_max]
					coord_sub, feat_sub, idx_sub = coord_part[idx_crop], feat_part[idx_crop], idx_part[idx_crop]
					# update potentials
					dist = dist[idx_crop]
					delta = np.square(1 - dist / np.max(dist))
					coord_p[idx_crop] += delta
					# prepare cropped input into baches - centralized, 0-1 rgb
					coord_sub, feat_sub = input_normalize(coord_sub, feat_sub)
					# finish current input sample
					idx_list.append(idx_sub), coord_list.append(coord_sub), feat_list.append(feat_sub), offset_list.append(idx_sub.size)
					# check newly covered voxels
					idx_uni = np.unique(np.concatenate((idx_uni, idx_sub)))

			else:
				# print(f"{i} ELSE IF shape not exceeds")
				# direct processing the full voxels in one sample
				coord_part, feat_part = input_normalize(coord_part, feat_part)
				idx_list.append(idx_part), coord_list.append(coord_part), feat_list.append(feat_part), offset_list.append(idx_part.size)

		batch_num = int(np.ceil(len(idx_list) / args.batch_size_test))
		for i in range(batch_num):
			# consrtuct batch from multiple input samples
			s_i, e_i = i * args.batch_size_test, min((i + 1) * args.batch_size_test, len(idx_list))
			idx_part, coord_part, feat_part, offset_part = idx_list[s_i:e_i], coord_list[s_i:e_i], feat_list[s_i:e_i], offset_list[s_i:e_i]
			idx_part = np.concatenate(idx_part)
			coord_part = torch.FloatTensor(np.concatenate(coord_part)).cuda(non_blocking=True)  # [BxN, 3]
			feat_part = torch.FloatTensor(np.concatenate(feat_part)).cuda(non_blocking=True)    # [BxN, d=3]
			offset_part = torch.IntTensor(np.cumsum(offset_part)).cuda(non_blocking=True)       # [BxN]
			with torch.no_grad():
				inputs = {'points': coord_part, 'features': feat_part, 'offset': offset_part}
				pred_part, _ = model(inputs)  # (n, k)
				pred_part = pred_part.detach().cpu().numpy()
			curr_dict['pred'][idx_part, ...] += pred_part[:, ...]
			curr_dict['pred_last'][idx_part, ...] = pred_part[:, ...]
			torch.cuda.empty_cache()

		print("Pred shape:", curr_dict['pred'].shape)
		pred_res = torch.Tensor(curr_dict['pred']).cuda(non_blocking=True)
		pred_res = pred_res.max(1)[1].data.detach().cpu().numpy()

		new_arr = np.zeros((coord.shape[0], 7))
		new_arr[:, 0:3] = coord[:, ...]
		new_arr[:, 3:6] = feat[:, ...]
		new_arr[:, 6] = pred_res[:]
		print("Save shape:", new_arr.shape)
		np.save(f"{spath}/RES_{item_file}.npy", new_arr)

		end_time = time.time()
		print("Time elapsed:", abs(end_time - start_time))

# ===================================
# Inference for one data
# ===================================

def process_data(data, args):
	data = normalize_shape(data)

	if(data.shape[0] > 400000):
		data, origin_idx = sample_data(data, 400000)
	else:
		origin_idx = np.arange(data.shape[0])
	coord, feat, label = data[:, :3], data[:, 3:6], data[:, 6]

	print("Data shape:", data.shape)

	idx_data = []
	if args.voxel_size:
		coord, feat = input_normalize(coord, feat)
		idx_sort, count = voxelize(coord, args.voxel_size, mode=1)  # sorted idx & cnts of entire cloud
		for i in range(count.max()):
			# enumerate through the max num of pts in the same voxel - [N_vx]
			idx_select = np.cumsum(np.insert(count, 0, 0)[0:-1]) + i % count
			idx_part = idx_sort[idx_select]
			idx_data.append(idx_part)
	else:
		idx_data.append(np.arange(label.shape[0]))
	return coord, feat, origin_idx, idx_data

def infer_one(data, config_file):
	cfg = config.load_cfg_from_cfg_file(config_file)
	cfg = config.CfgNode(cfg, default='')

	model, _, _ = prep_test(cfg)

	start_time = time.time()

	coord, feat, origin_idx, idx_data = process_data(data, cfg)
	idx_size = len(idx_data)
	print("Index data len:", idx_size)

	curr_dict = {
		'pred' : np.zeros((coord.shape[0], 13)),
		'pred_last' : np.zeros((coord.shape[0], 13))
	}

	idx_list, coord_list, feat_list, offset_list  = [], [], [], []
	for i in range(idx_size):  # for each enmeration of the full (potentially voxelized) cloud
		#logger.info('{}/{}: {}/{}/{}, {}'.format(idx + 1, len(data_list), i + 1, idx_size, idx_data[0].shape[0], item))
		idx_part = idx_data[i]

		# select points from full cloud into voxels
		coord_part, feat_part = coord[idx_part], feat[idx_part]

		if cfg.voxel_max and coord_part.shape[0] > cfg.voxel_max:
			# print(f"{i} IF shape exceeds")
			# if exceeding test/val voxel num - spatially regular gen
			coord_p, idx_uni, _ = np.random.rand(coord_part.shape[0]) * 1e-3, np.array([]), 0
			# until all voxels of current cloud covered
			while idx_uni.size != idx_part.shape[0]:
				# select center (min potentials)
				init_idx = np.argmin(coord_p)
				# crop by distance
				dist = np.sum(np.power(coord_part - coord_part[init_idx], 2), 1)
				idx_crop = np.argsort(dist)[:cfg.voxel_max]
				coord_sub, feat_sub, idx_sub = coord_part[idx_crop], feat_part[idx_crop], idx_part[idx_crop]
				# update potentials
				dist = dist[idx_crop]
				delta = np.square(1 - dist / np.max(dist))
				coord_p[idx_crop] += delta
				# prepare cropped input into baches - centralized, 0-1 rgb
				coord_sub, feat_sub = input_normalize(coord_sub, feat_sub)
				# finish current input sample
				idx_list.append(idx_sub), coord_list.append(coord_sub), feat_list.append(feat_sub), offset_list.append(idx_sub.size)
				# check newly covered voxels
				idx_uni = np.unique(np.concatenate((idx_uni, idx_sub)))

		else:
			# print(f"{i} ELSE IF shape not exceeds")
			# direct processing the full voxels in one sample
			coord_part, feat_part = input_normalize(coord_part, feat_part)
			idx_list.append(idx_part), coord_list.append(coord_part), feat_list.append(feat_part), offset_list.append(idx_part.size)

	batch_num = int(np.ceil(len(idx_list) / cfg.batch_size_test))
	for i in range(batch_num):
		# consrtuct batch from multiple input samples
		s_i, e_i = i * cfg.batch_size_test, min((i + 1) * cfg.batch_size_test, len(idx_list))
		idx_part, coord_part, feat_part, offset_part = idx_list[s_i:e_i], coord_list[s_i:e_i], feat_list[s_i:e_i], offset_list[s_i:e_i]
		idx_part = np.concatenate(idx_part)
		coord_part = torch.FloatTensor(np.concatenate(coord_part)).cuda(non_blocking=True)  # [BxN, 3]
		feat_part = torch.FloatTensor(np.concatenate(feat_part)).cuda(non_blocking=True)    # [BxN, d=3]
		offset_part = torch.IntTensor(np.cumsum(offset_part)).cuda(non_blocking=True)       # [BxN]
		with torch.no_grad():
			inputs = {'points': coord_part, 'features': feat_part, 'offset': offset_part}
			pred_part, _ = model(inputs)  # (n, k)
			pred_part = pred_part.detach().cpu().numpy()
		curr_dict['pred'][idx_part, ...] += pred_part[:, ...]
		curr_dict['pred_last'][idx_part, ...] = pred_part[:, ...]
		torch.cuda.empty_cache()

	print("Pred shape:", curr_dict['pred'].shape)
	pred_res = torch.Tensor(curr_dict['pred']).cuda(non_blocking=True)
	pred_res = pred_res.max(1)[1].data.detach().cpu().numpy()

	new_arr = np.zeros((coord.shape[0], 7))
	new_arr[:, 0:3] = coord[:, ...]
	new_arr[:, 3:6] = feat[:, ...]
	new_arr[:, 6] = pred_res[:]
	print("Save shape:", new_arr.shape)

	result_data = np.zeros((coord.shape[0], 2))
	result_data[:, 0] = origin_idx[:, ...]
	result_data[:, 1] = pred_res[:]

	end_time = time.time()
	print("Time elapsed:", abs(end_time - start_time))

	return (result_data, new_arr)


if __name__ == '__main__':
	main()
